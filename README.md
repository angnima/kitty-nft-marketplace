# CryptoKitty

![Minting Factory](/assets/images/factory.jpg)

This project contains both frontend and solidity contract for the creation of CryptoKitty.
The components include minting new generation of kitty through the factory using various sliders properties 
and breed page where you can breed multiple kitty into another kitty with mixed dna sequence. 

## Explore Minted Kitties
![Explore Kitties](/assets/images/explore.jpg)

## Breeding Kitty:
![Breeding Factory](/assets/images/breed.jpg)

## To run the project:
- Install, Run and Create a project on Ganache.
- ```npm install``` in solidity directory.
- ```truffle migrate``` in solidity directory to migrate the contract.
- Copy the contract address for KittyContract.sol.
- Paste the contract address to index.js in assets directory. (Replace contractAddress variable)
- Run ```python3 -m http.server 8000``` on cmd.
- Start minting your own kitties!
