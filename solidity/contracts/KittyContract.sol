// SPDX-License-Identifier: MIT
pragma solidity >=0.5.0 < 0.8.11;

import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./IERC721.sol";
import "./IERC721Receiver.sol";

contract KittyContract is IERC721, Ownable {

  using SafeMath for uint256;
  
  string private _name;
  string private _symbol;

  uint256 public gen0Counter;
  uint256 public gen0MintLimit = 9;

  bytes4 private constant ERC721_INTERFACE = 0x80ac58cd;
  bytes4 private constant ERC165_INTERFACE = 0x01ffc9a7;

  bytes4 private constant ERC721_Receive_Rule = bytes4(keccak256("onERC721Received(address,address,uint256,bytes)"));

  struct Kitty {
    uint256 dna;
    uint64 spawnedAt;
    uint32 momId;
    uint32 dadId;
    uint16 generation;
  }

  Kitty[] kitties;

  mapping(address => uint256) public balances;
  mapping(uint256 => address) public owners;
  
  // Token Id to Address approval for transfer
  mapping(uint256 => address) public _tokenApprovals;
  // Approval for contract/address to transfer all wallet address's tokens 
  mapping(address => mapping(address => bool)) private _operatorApprovals;

  event KittySpawned(uint256 kittyId, uint256 dna, address owner, uint256 momId, uint256 dadId);

  constructor(string memory name_, string memory symbol_) {
    _name = name_;
    _symbol = symbol_;
  }

  function supportsInterface(bytes4 _interfaceId) external view returns (bool) {
    return (_interfaceId == ERC721_INTERFACE || _interfaceId == ERC165_INTERFACE);
  }

  function balanceOf(address owner) external override view returns (uint256 balance) {
    balance = balances[owner];
  }

  function totalSupply() external override view returns (uint256 total) {
    total = kitties.length;
  }

  function name() external override view returns (string memory tokenName) {
    tokenName = _name;
  }

  function symbol() external override view returns (string memory tokenSymbol) {
    tokenSymbol = _symbol;
  }

  function _exists(uint256 _tokenId) internal view returns (bool) {
    return (_tokenId < kitties.length);
  }

  function ownerOf(uint256 _tokenId) public override view returns (address owner) {
    require(_exists(_tokenId), "TokenId does not exists!");
    owner = owners[_tokenId];
  }

  function createKittyGen0(uint256 _dna) public onlyOwner {
    require(gen0Counter < gen0MintLimit, "Minting Limit reached for Gen0");

    gen0Counter = gen0Counter.add(1);
    
    _createKitty(_dna, 0, 0, 0, msg.sender);
  }

  function _createKitty(
    uint256 _dna, 
    uint256 _momId, 
    uint256 _dadId, 
    uint256 _generation, 
    address _owner
  ) private returns (uint256){

    Kitty memory kitty = Kitty({
      dna: _dna,
      spawnedAt: uint64(block.timestamp),
      momId: uint32(_momId),
      dadId: uint32(_dadId),
      generation: uint16(_generation)
    });

    kitties.push(kitty);
    uint256 newKittyId = (kitties.length).sub(1);
    
    _transfer(address(0), _owner, newKittyId);
    emit KittySpawned(newKittyId, _dna, _owner, _momId, _dadId);
    
    return newKittyId;
  }

  function breedKitty(uint256 _dadId, uint256 _momId) public returns (uint256) {
    require(_exists(_dadId) && _exists(_momId), "NFT does not exists!");
    require(owners[_dadId] == msg.sender && owners[_momId] == msg.sender, "NFT does not belong to the address!");
    require(kitties[_dadId].generation == kitties[_momId].generation, "Generation must match to breed!");
    uint256 newDna = _mixDna(kitties[_dadId].dna, kitties[_momId].dna);
    uint256 gen = uint256(kitties[_dadId].generation).add(1);
    return _createKitty(newDna, _momId, _dadId, gen, msg.sender);
  }

  function _mixDna(uint256 _dadDna, uint256 _momDna) public returns (uint256) {
    uint256 dadPart = _dadDna/10**8;
    uint256 momPart = _momDna % 10**8;

    uint256 newDna = dadPart * 10**8;
    newDna = newDna + momPart;
    return newDna;
  }

  function getKitty(uint256 _tokenId) external view returns (
    uint256 dna,
    uint256 spwanedAt,
    uint256 momId,
    uint256 dadId,
    uint256 generation
  ){
    require(_exists(_tokenId), "TokenId does not exists!");

    Kitty storage kitty = kitties[_tokenId];

    return (kitty.dna, uint256(kitty.spawnedAt), uint256(kitty.momId), uint256(kitty.dadId), uint256(kitty.generation));
  }

  function transfer(address to, uint256 tokenId) external override {
    require(to != address(0), "Recieving address cannot be dead address!");
    require(to != address(this), "Cannot send to the contract!");
    require(owners[tokenId] == msg.sender, "Token Id must belong to the message sender!");

    _transfer(msg.sender, to, tokenId);
  }

  function approve(address _approved, uint256 _tokenId) external override {
    require(owners[_tokenId] == msg.sender || _operatorApprovals[ownerOf(_tokenId)][msg.sender], "Not allowed to approve asset you dont own!");
    _approve(_approved, _tokenId);

    emit Approval(msg.sender, _approved, _tokenId);
  }

  function _approve(address _approved, uint256 _tokenId) internal {
    _tokenApprovals[_tokenId] = _approved;
  }

  function setApprovalForAll(address _operator, bool _approved) external override {
    _operatorApprovals[msg.sender][_operator] = _approved;

    emit ApprovalForAll(msg.sender, _operator, _approved);
  }

  function getApproved(uint256 _tokenId) external override view returns (address) {
    require(_exists(_tokenId), "TokenId does not exists!");
    return _tokenApprovals[_tokenId];
  }

  function isApprovedForAll(address _owner, address _operator) public view returns (bool) {
    return _operatorApprovals[_owner][_operator];
  }

  function _isApprovedOrOwner(address _spender, uint256 _tokenId, address _to) internal view returns (bool) {
    require(_to != address(0), "Reciever must not be dead address!");
    require(_exists(_tokenId), "TokenId does not exists!");

    address owner = owners[_tokenId];
    return (_spender == owner || _tokenApprovals[_tokenId] == _spender || isApprovedForAll(owner, _spender));
  }

  function safeTransferFrom(address _from, address _to, uint256 _tokenId, bytes memory _data) public {
    require(_isApprovedOrOwner(_from, _tokenId, _to), "Must be owner or an approved operator to transfer the NFT!");

    _safeTransfer(_from, _to, _tokenId, _data);
  }

  function safeTransferFrom(address _from, address _to, uint256 _tokenId) external {
    safeTransferFrom(_from, _to, _tokenId, "");
  }

  function transferFrom(address _from, address _to, uint256 _tokenId) external {
    require(_isApprovedOrOwner(_from, _tokenId, _to), "Must be owner or an approved operator to transfer the NFT!");

    _safeTransfer(_from, _to, _tokenId, "");
  }

  function _safeTransfer(address _from, address _to, uint256 _tokenId, bytes memory _data) internal {
    _transfer(_from, _to, _tokenId);
    require(_checkIfERC721IsReceived(_from, _to, _tokenId, _data), "ERC721 not receivable!");
  }

  function _transfer(address _from, address _to, uint256 _tokenId) internal {
    if(_from != address(0)) {
      balances[_from] = balances[_from].sub(1);
    }

    owners[_tokenId] = _to;
    balances[_to] = balances[_to].add(1);

    _approve(address(0), _tokenId);

    emit Transfer(_from, _to, _tokenId);
  }

  function _checkIfERC721IsReceived(address _from, address _to, uint256 _tokenId, bytes memory _data) internal returns (bool) {
    if(!_isContract(_to)) {
      return true;
    }
    bytes4 returnData = IERC721Receiver(_to).onERC721Received(msg.sender, _from, _tokenId, _data);
    return (returnData == ERC721_Receive_Rule);
  }

  function _isContract(address _to) internal view returns (bool) {
    uint32 size;
    assembly {
      size := extcodesize(_to)
    }
    return size > 0;
  }

}
