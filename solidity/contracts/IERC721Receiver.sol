pragma solidity >=0.5.0 < 0.8.11;

interface IERC721Receiver {

    function onERC721Received(address _operator, address _from, uint256 _tokenId, bytes calldata _data) external returns (bytes4);

}