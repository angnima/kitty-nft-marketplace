var web3;

if(window.ethereum) {
    web3 = new Web3(window.ethereum)
}else if(window.web3) {
    web3 = new Web3(Web3.givenProvider)
}

var instance;
var user;
var contractAddress = "0x9C9753a481D13757cC8632d51E1AeD8Ab8934530";

let user_kitties = [];

$(document).ready(async function() {
    const accounts = await window.ethereum.request({ method: "eth_requestAccounts" });

    if(accounts.length > 0) {
        instance = new web3.eth.Contract(abi, contractAddress, { from: user });
        user = accounts[0];
    }

    function createKitty() {
        let dna = getDna();
        
        instance.methods.createKittyGen0(dna).send({from: user}, function(error, txHash) {
            if(error) {
                console.log(error)
            }else if(txHash) {
                checkSpawnedEvents()
            }
        })
    }

    async function checkSpawnedEvents() {
        instance.events.KittySpawned({
            fromBlock: await web3.eth.getBlockNumber()
        }, function(error, event) {
            if(error) {
                console.log(error)
            }else if(event) {
                let tokenId = event.returnValues.kittyId;
                alert(`Kitty successfully created with TokenId: ${ tokenId }`)
            }
        })
    }

    $("#createBtn").click(function() {
        createKitty()
    })

    async function fetchUserKitties(breed = false) {
        let supply = await instance.methods.totalSupply().call();
        for(let i = 0; i < supply; i++) {
            let kitty = await instance.methods.getKitty(i).call();
            user_kitties.push({
                id: i,
                dna: kitty.dna,
                generation: kitty.generation,
                dadId: kitty.dadId,
                momId: kitty.momId,
                spwanedAt: kitty.spwanedAt
            })
        }
        if(breed) {
            appendKittyForBreed()
        }else {
            displayKitties()
        }
    }

    if(window.location.pathname == "/explore.html") {
        fetchUserKitties()
    }else if(window.location.pathname == "/breed.html") {
        fetchUserKitties(true)
    }

    function appendKittyForBreed() {
        for(let i = 0; i < user_kitties.length; i++) {
            let dna = user_kitties[i].dna
            $("#dadKitty").append(`
                <option value="${ user_kitties[i].id }">Dna: ${ dna } (Gen: ${ user_kitties[i].generation })</option>
            `)

            $("#momKitty").append(`
                <option value="${ user_kitties[i].id }">Dna: ${ dna } (Gen: ${ user_kitties[i].generation })</option>
            `)
        }
    }

    function displayKitties(displayAt = null) {
        for(let i = 0; i < user_kitties.length; i++) {
            let dna = user_kitties[i].dna

            let headColor, eyesColor, earsColor, tummyColor, eyesShape, stripesColor, decorationPattern, decorationMidcolor, decorationSidescolor, animation;
            headColor = parseInt(dna.substr(0,2));
            eyesColor = parseInt(dna.substr(2,2));
            earsColor = parseInt(dna.substr(4,2));
            tummyColor = parseInt(dna.substr(6,2));
            stripesColor = parseInt(dna.substr(8,2));
            eyesShape = parseInt(dna.substr(10,1));
            decorationPattern = parseInt(dna.substr(11,1));
            decorationMidcolor = parseInt(dna.substr(12,2));
            decorationSidescolor = parseInt(dna.substr(14,2));
            animation = parseInt(dna.substr(16,1));
            
            let html = `
                <div class="col-lg-3 catBox" id="kitty-no-${ i }">

                    <div class="row">

                        <div id="cs-container light-b-shadow">
                            <div id="tail" style="border:15px solid #${ colors[earsColor] };"></div>
                            <div id="tail-mask"></div>
                            <div id="tail-end" style="background-color:#${ colors[earsColor] };"></div>
                            
                            <div id="body" style="background-color:#${ colors[headColor] };">
                                <div class="ear" style="background-color:#${ colors[earsColor] };" id="ear-left">
                                    <div class="ear-inner" style="background-color:#${ colors[tummyColor] };" id="ear-inner-left"></div>
                                </div>
                                <div class="ear" style="background-color:#${ colors[earsColor] };" id="ear-right">
                                    <div class="ear-inner" style="background-color:#${ colors[tummyColor] };" id="ear-inner-right"></div>
                                </div>
                                
                                <div id="mask" style="background-color:#${ colors[headColor] };"></div>
                                
                                <div id="patch">
                                    <div class="fur" style="background-color:#${ colors[decorationSidescolor] };"></div>
                                    <div class="fur" style="background-color:#${ colors[decorationMidcolor] };"></div>
                                    <div class="fur" style="background-color:#${ colors[decorationSidescolor] };"></div>
                                </div>
                                
                                <div id="eyes">
                                    <div class="eye" style="background-color:#${ colors[eyesColor] };" id="eye-left">
                                        <div class="tears"></div>
                                        <div class="tears"></div>

                                        <div class="shine" id="shine-left"></div>
                                    </div>
                                    <div class="eye" style="background-color:#${ colors[eyesColor] };" id="eye-right">
                                        <div class="tears"></div>
                                        <div class="tears"></div>

                                        <div class="shine" id="shine-right"></div>
                                    </div>
                                </div>
                                
                                <div id="whisk-left">
                                    <div class="whisker" id="whisk-one"></div>
                                    <div class="whisker"></div>
                                    <div class="whisker" id="whisk-three"></div>
                                </div>
                                
                                <div id="nose"></div>
                                
                                <div id="whisk-right">
                                    <div class="whisker" id="whisk-four"></div>
                                    <div class="whisker"></div>
                                    <div class="whisker" id="whisk-six"></div>
                                </div>
                                
                                <div id="smile">
                                    <div id="smile-left-align">
                                        <div id="smile-left" style="background-color:#${ colors[headColor] };"></div>
                                        <div id="mask-left" style="background-color:#${ colors[headColor] };"></div>
                                    </div>
                                    
                                    <div id="smile-right-align">
                                        <div id="smile-right" style="background-color:#${ colors[headColor] };"></div>
                                        <div id="mask-right" style="background-color:#${ colors[headColor] };"></div>
                                    </div>
                                </div>
                                
                                <div id="tongue" class="tongueAnimation"></div>
                                <div id="tummy" style="background-color:#${ colors[tummyColor] };"></div>
                            </div>
                        </div>
                    </div>
                    <div class="dnaDiv" id="catDNA">
                        <b>
                            DNA: ${ user_kitties[i].dna } <br/>
                            Gen: ${ user_kitties[i].generation }
                        </b>
                    </div>
                </div>`
            if(displayAt == null) {
                $("#kittyContain").append(html);
            }else {
                $(displayAt).append(html);
            }
            
            renderDisplayKitty(`#kitty-no-${i}`, animation, eyesShape, decorationPattern);
        }
    }

    function renderDisplayKitty(kitty_class, animation, eyesShape, decorationPattern) {
        if(animation == 1) {

        }else if(animation == 2) {
            $(kitty_class + " .shine").addClass("eyesMovement");
            $(kitty_class + " #animationCode").html("Eyes");
        }else if(animation == 3) {
            $(kitty_class + " .ear").addClass("earsMovement");
            $(kitty_class + " #animationCode").html("Ears");
        }else if(animation == 4) {
            $(kitty_class + " .tears").addClass("tearsMovement");
            $(kitty_class + " #smile-left").addClass("flip-item");
            $(kitty_class + " #mask-left").addClass("flip-item");
            $(kitty_class + " #smile-right").addClass("flip-item");
            $(kitty_class + " #mask-right").addClass("flip-item");

            $(kitty_class + " #mask-left").addClass("margin-15pc");
            $(kitty_class + " #mask-right").addClass("margin-15pc");
            $(kitty_class + " #tongue").addClass("hide");

            $(kitty_class + " #animationCode").html("Tears");
        }else if(animation == 5) {
            $(kitty_class + " #tongue").addClass("tongueAnimation");
            $(kitty_class + " #animationCode").html("Tongue");
        }

        if(eyesShape == 3) {
            $(".eye").css("border-top", "6px solid");
        }else if(eyesShape == 2) {
            $(".eye").css("border-bottom", "6px solid");
        }else {
            $(".eye").css("border-top", "none");
        }

        switch (parseInt(decorationPattern)) {
            case 1: 
                break;
            case 2:
                $(".fur:first").css("transform", "translate(9px, 10px)")
                $('#DStylecode').html('Ping');
                break;
            case 3:
                $(".fur:first").css("transform", "translate(9px, 10px)")
                $(".fur:last").css("transform", "rotate(90deg) translate(3px, 9px)");
                $('#DStylecode').html('Cross');
                break;
            case 4:
                $(".fur:first").css("transform", "rotate(90deg) translate(6px, -9px)");
                $(".fur:last").css("transform", "rotate(90deg) translate(3px, 9px)");
                $('#DStylecode').html('Pyramid');
                break;
            case 5:
                $(".fur:first").css("transform", "translate(9px, 20px)");
                $(".fur:last").css("transform", "rotate(90deg) translate(3px, 9px)");
                $('#DStylecode').html('Guru');
                break;
            case 6:
                $(".fur:first").css("transform", "translate(9px, 10px) rotate(90deg)");
                $(".fur:last").css("transform", "translate(-9px, 6px)");
                $('#DStylecode').html('Gunner');
                break;
            default:
                break;
        }
    }

    $("#breedBtn").click(async function() {
        let dadId = $("#dadKitty").val();
        let momId = $("#momKitty").val();
        
        if(dadId == momId) return

        try {
            await instance.methods.breedKitty(dadId, momId).send({from: user})
            alert("Kitty successfully bred!")
            window.location = "/explore.html"
        } catch (err) {
            alert("There was an error: ", err)
            console.log(err)
        }
    })
})

