
//Random color
function getColor() {
    var randomColor = Math.floor(Math.random() * 16777215).toString(16);
    return randomColor
}

function genColors(){
    var colors = []
    for(var i = 10; i < 99; i ++){
      var color = getColor()
      colors[i] = color
    }
    return colors
}

//This function code needs to modified so that it works with Your cat code.
function headColor(color,code) {
    $('#body, #mask, #mask-right, #smile-right, #mask-left, #smile-left').css('background-color', '#' + color)  //This changes the color of the cat
    // $("#tail").css("border", "15px solid #" + color);
    // $("#tail-end").css("background-color", "#" + color);
    $('#headcode').html('code: '+code) //This updates text of the badge next to the slider
    $('#dnabody').html(code) //This updates the body color part of the DNA that is displayed below the cat
}

function eyeColor(color,code) {
    $('.eye').css('background-color', '#' + color)  //This changes the color of the cat
    $('#eyecode').html('code: '+code) //This updates text of the badge next to the slider
    $('#dnaeyes').html(code) //This updates the body color part of the DNA that is displayed below the cat
}


function ETColor(color,code) {
    $("#tail").css("border", "15px solid #" + color);
    $("#tail-end").css("background-color", "#" + color);
    $(".ear").css("background-color", "#" + color);
    
    $('#ETcode').html('code: '+code) //This updates text of the badge next to the slider
    $('#dnaears').html(code) //This updates the body color part of the DNA that is displayed below the cat
}

function IEBColor(color,code) {
    $(".ear-inner").css("background-color", "#" + color);
    $("#tummy").css("background-color", "#" + color);
    
    $('#IEBcode').html('code: '+code) //This updates text of the badge next to the slider
    $('#dnabelly').html(code) //This updates the body color part of the DNA that is displayed below the cat
}

function SWColor(color,code) {
    $(".fur").css("background-color", "#" + color);
    
    $('#SWcode').html('code: ' + code); //This updates text of the badge next to the slider
    $('#dnastripes').html(code) //This updates the body color part of the DNA that is displayed below the cat
}

function DMColor(color, code) {
    $(".fur:odd").css("background-color", "#" + color);

    $('#DMcode').html('code: ' + code);
    $('#dnadecorationMid').html(code);
}

function DSideColor(color, code) {
    $(".fur:even").css("background-color", "#" + color);

    $('#DSidecode').html('code: ' + code);
    $('#dnadecorationSides').html(code);
}

function changeAnimation(code) {
    code = parseInt(code);

    $("#dnaanimation").html(code);
    
    removeAllAnimations()

    switch (code) {
        case 1: 
            $("#animationCode").html("None");
            break;
        case 2: 
            animationType2();
            break;
        case 3: 
            animationType3();
            break;
        case 4: 
            animationType4();
            break;
        case 5: 
            animationType5();
            break;
        default: 
            break;
    }
}

function animationType2() {
    $(".shine").addClass("eyesMovement");
    $("#animationCode").html("Eyes");
}

function animationType3() {
    $(".ear").addClass("earsMovement");
    $("#animationCode").html("Ears");
}

function animationType4() {
    $(".tears").addClass("tearsMovement");
    $("#smile-left, #mask-left, #smile-right, #mask-right").addClass("flip-item");
    $("#mask-left, #mask-right").addClass("margin-15pc");
    $("#tongue").addClass("hide");

    $("#animationCode").html("Tears");
}

function animationType5() {
    $("#tongue").addClass("tongueAnimation");
    $("#animationCode").html("Tongue");
}

function removeAllAnimations() {
    $(".shine").removeClass("eyesMovement");

    $(".ear").removeClass("earsMovement");

    $(".tears").removeClass("tearsMovement");
    $("#smile-left, #mask-left, #smile-right, #mask-right").removeClass("flip-item");
    $("#mask-left, #mask-right").removeClass("margin-15pc");
    $("#tongue").removeClass("hide");

    $("#tongue").removeClass("tongueAnimation");
}

function EyeShape(code) {
    normalEyes()
    $("#dnashape").html(code)
    if(code == 3) {
        $(".eye").css("border-top", "6px solid");
        $("#eyesShapecode").html("Sleepy");
    }else if(code == 2) {
        $(".eye").css("border-bottom", "6px solid");
        $("#eyesShapecode").html("Sus");
    }else {
        $(".eye").css("border-top", "none");
        $("#eyesShapecode").html("Basic");
    }
}

function changeDStyle(style) {
    style = parseInt(style)

    $("#dnadecoration").html(style)
    switch (style) {
        case 1: 
            normalDStyle()
            $('#DStylecode').html('Basic');
            break;
        case 2:
            normalDStyle()
            $(".fur:first").css("transform", "translate(9px, 10px)")
            $('#DStylecode').html('Ping');
            break;
        case 3:
            normalDStyle()
            $(".fur:first").css("transform", "translate(9px, 10px)")
            $(".fur:last").css("transform", "rotate(90deg) translate(3px, 9px)");
            $('#DStylecode').html('Cross');
            break;
        case 4:
            normalDStyle()
            $(".fur:first").css("transform", "rotate(90deg) translate(6px, -9px)");
            $(".fur:last").css("transform", "rotate(90deg) translate(3px, 9px)");
            $('#DStylecode').html('Pyramid');
            break;
        case 5:
            normalDStyle()
            $(".fur:first").css("transform", "translate(9px, 20px)");
            $(".fur:last").css("transform", "rotate(90deg) translate(3px, 9px)");
            $('#DStylecode').html('Guru');
            break;
        case 6:
            normalDStyle()
            $(".fur:first").css("transform", "translate(9px, 10px) rotate(90deg)");
            $(".fur:last").css("transform", "translate(-9px, 6px)");
            $('#DStylecode').html('Gunner');
            break;
        default:
            normalDStyle()
            $('#DStylecode').html('Basic');
            break;
    }
}

//###################################################
//Functions below will be used later on in the project
//###################################################
function eyeVariation(num) {
    $('#dnashape').html(num)
    switch (num) {
        case 1:
            normalEyes()
            $('#eyeName').html('Basic')
            break
    }
}

function decorationVariation(num) {
    $('#dnadecoration').html(num)
    switch (num) {
        case 1:
            $('#decorationName').html('Basic')
            normaldecoration()
            break
    }
}

async function normalEyes() {
    await $('.eye').css('border', 'none')
}

async function normalDStyle() {
    await $(".fur").css("transform", "none");
}

async function normaldecoration() {
    //Remove all style from other decorations
    //In this way we can also use normalDecoration() to reset the decoration style
    $('.cat__head-dots').css({ "transform": "rotate(0deg)", "height": "48px", "width": "14px", "top": "1px", "border-radius": "0 0 50% 50%" })
    $('.cat__head-dots_first').css({ "transform": "rotate(0deg)", "height": "35px", "width": "14px", "top": "1px", "border-radius": "50% 0 50% 50%" })
    $('.cat__head-dots_second').css({ "transform": "rotate(0deg)", "height": "35px", "width": "14px", "top": "1px", "border-radius": "0 50% 50% 50%" })
}
