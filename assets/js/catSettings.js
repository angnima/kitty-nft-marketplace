
var colors = Object.values(allColors())

var defaultDNA = {
    "headcolor" : 10,
    "tummyColor" : 55,
    "eyesColor" : 89,
    "earsColor" : 10,
    "stripesColor" : 90,
    //Cattributes
    "eyesShape" : 1,
    "decorationPattern" : 1,
    "decorationMidcolor" : 90,
    "decorationSidescolor" : 14,
    "animation" :  1,
    "lastNum" :  1
}

// when page load
$( document ).ready(function() {
  if(window.location.pathname != "/explore.html") {
    $('#dnabody').html(defaultDNA.headColor);
    $('#dnaeyes').html(defaultDNA.eyesColor);
    $('#dnaears').html(defaultDNA.earsColor);
    $('#dnabelly').html(defaultDNA.tummyColor);
    $('#dnastripes').html(defaultDNA.stripesColor);
      
    $('#dnashape').html(defaultDNA.eyesShape)
    $('#dnadecoration').html(defaultDNA.decorationPattern)
    $('#dnadecorationMid').html(defaultDNA.decorationMidcolor)
    $('#dnadecorationSides').html(defaultDNA.decorationSidescolor)
    $('#dnaanimation').html(defaultDNA.animation)
//   $('#dnaspecial').html(defaultDNA.lastNum)
    renderCat(defaultDNA)
  }
});

function getDna(){
    var dna = ''
    dna += $('#dnabody').html()
    dna += $('#dnaeyes').html()
    dna += $('#dnaears').html()
    dna += $('#dnabelly').html()
    dna += $('#dnastripes').html()
    dna += $('#dnashape').html()
    dna += $('#dnadecoration').html()
    dna += $('#dnadecorationMid').html()
    dna += $('#dnadecorationSides').html()
    dna += $('#dnaanimation').html()

    return dna;
}

function renderCat(dna){
    $('#bodycolor').val(dna.headcolor)
    headColor(colors[dna.headcolor],dna.headcolor)

    $('#eyecolor').val(dna.eyesColor)
    eyeColor(colors[dna.eyesColor],dna.eyesColor)

    $('#ETcolor').val(dna.earsColor)
    ETColor(colors[dna.earsColor],dna.earsColor)

    $('#IEBcolor').val(dna.tummyColor)
    IEBColor(colors[dna.tummyColor],dna.tummyColor)

    $('#eyesShape').val(dna.eyesShape)
    EyeShape(dna.eyesShape)

    $('#SWcolor').val(dna.stripesColor)
    SWColor(colors[dna.stripesColor],dna.stripesColor)

    $('#DStyle').val(dna.decorationPattern)
    changeDStyle(dna.decorationPattern)

    $('#DMcolor').val(dna.decorationMidcolor)
    DMColor(colors[dna.decorationMidcolor], dna.decorationMidcolor);

    $("#DSidecolor").val(dna.decorationSidescolor);
    DSideColor(colors[dna.decorationSidescolor], dna.decorationSidescolor);

    $("#animateCat").val(dna.animation);
    changeAnimation(dna.animation);
}

// Changing cat colors
$('#bodycolor').change(()=>{
    var colorVal = $('#bodycolor').val()
    headColor(colors[colorVal], colorVal)
})

$('#eyecolor').change(()=>{
  var colorVal = $('#eyecolor').val()
  eyeColor(colors[colorVal], colorVal)
})

$('#ETcolor').change(()=>{
  var colorVal = $('#ETcolor').val()
  ETColor(colors[colorVal], colorVal)
})

$('#IEBcolor').change(()=>{
  var colorVal = $('#IEBcolor').val()
  IEBColor(colors[colorVal], colorVal)
})

$('#eyesShape').change(()=>{
  var shapeVal = $('#eyesShape').val()
  EyeShape(shapeVal)
})

$('#DStyle').change(()=>{
  var StyleVal = $('#DStyle').val()
  changeDStyle(StyleVal)
})

$('#SWcolor').change(()=>{
  var colorVal = $('#SWcolor').val()
  SWColor(colors[colorVal], colorVal)
})

$('#DMcolor').change(()=>{
  var colorVal = $('#DMcolor').val()
  DMColor(colors[colorVal], colorVal)
})

$('#DSidecolor').change(()=>{
  var colorVal = $('#DSidecolor').val()
  DSideColor(colors[colorVal], colorVal)
})

$("#animateCat").change(() => {
  var animateVal = $("#animateCat").val()
  changeAnimation(animateVal);
})


// BTN Change

$("#randomBtn").click(getRandomKitty)

function getRandomKitty() {
  let headColor_dna = getRandomDnaNum();
  $('#bodycolor').val(headColor_dna)
  headColor(colors[headColor_dna], headColor_dna)

  let eyeColor_dna = getRandomDnaNum();
  $('#eyecolor').val(eyeColor_dna)
  eyeColor(colors[eyeColor_dna], eyeColor_dna)

  let ETColor_dna = getRandomDnaNum();
  $('#ETcolor').val(ETColor_dna)
  ETColor(colors[ETColor_dna], ETColor_dna)

  let IEBColor_dna = getRandomDnaNum();
  $('#IEBcolor').val(IEBColor_dna)
  IEBColor(colors[IEBColor_dna], IEBColor_dna)

  let eyeShape_dna = Math.floor(Math.random() * 3) + 1;
  $('#eyesShape').val(eyeShape_dna)
  EyeShape(eyeShape_dna)

  let SWColor_dna = getRandomDnaNum();
  $('#SWcolor').val(SWColor_dna)
  SWColor(colors[SWColor_dna], SWColor_dna)

  let DStyle_dna = Math.floor(Math.random() * 6) + 1;
  $('#DStyle').val(DStyle_dna)
  changeDStyle(DStyle_dna)

  let DMColor_dna = getRandomDnaNum();
  $('#DMcolor').val(DMColor_dna)
  DMColor(colors[DMColor_dna], DMColor_dna);

  let DSideColor_dna = getRandomDnaNum();
  $("#DSidecolor").val(DSideColor_dna);
  DSideColor(colors[DSideColor_dna], DSideColor_dna);

  let animate_dna = Math.floor(Math.random() * 5) + 1;
  $("#animateCat").val(animate_dna);
  changeAnimation(animate_dna);
}

function getRandomDnaNum() {
  return Math.floor(Math.random() * 89) + 10
}

$("#defaultBtn").click(() => {
  renderCat(defaultDNA)
})